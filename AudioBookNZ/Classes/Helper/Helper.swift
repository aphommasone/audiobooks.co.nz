//
//  Helper.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 15/02/18.
//  Copyright © 2018 Arnaud. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class Helper: NSObject {
        class var shared: Helper {
            struct Static {
                static let instance: Helper = Helper()
            }
            return Static.instance
        }
    
    func getAudioPathURL(download: AudioBookDownloads) -> URL? {
        do {
            let documentsURL = download.pathInDocuments()
            try documentsURL.checkResourceIsReachable()
            
            return documentsURL
            
        } catch let error as NSError {
            print(error)
        }
        return nil
    }
    
    func retrieveChaptersM4B(asset: AVAsset) -> [Chapter]
    {
        var chapterList: [Chapter] = []
        let chapterIndex = 1
        
        let locales = asset.availableChapterLocales
        for locale in locales {
            let chapters = asset.chapterMetadataGroups(withTitleLocale: locale, containingItemsWithCommonKeys: [AVMetadataCommonKeyArtwork])
            
            for chapterMetadata in chapters {
                
                let chapter = Chapter(title: AVMetadataItem.metadataItems(from: chapterMetadata.items, withKey: AVMetadataCommonKeyTitle, keySpace: AVMetadataKeySpaceCommon).first?.value?.copy(with: nil) as? String ?? "Chapter \(chapterIndex)",
                    start: Int(CMTimeGetSeconds(chapterMetadata.timeRange.start)),
                    duration: Int(CMTimeGetSeconds(chapterMetadata.timeRange.duration)),
                    index: chapterIndex)
                
                chapterList.append(chapter)
                print("chapter : \(chapter)")
            }
            
        }
        
        return chapterList
    }
    
    //This returns song length
    func calculateTimeFromNSTimeInterval(_ duration:TimeInterval) ->(hour: String, minute:String, second:String){
        // let hour_   = abs(Int(duration)/3600)
        let hour_ = abs(Int((duration/60/60).truncatingRemainder(dividingBy: 60)))
        let minute_ = abs(Int((duration/60).truncatingRemainder(dividingBy: 60)))
        let second_ = abs(Int(duration.truncatingRemainder(dividingBy: 60)))
        
        // var hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
        let hour = hour_ > 9 ? "\(hour_)" : "0\(hour_)"
        let minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
        let second = second_ > 9 ? "\(second_)" : "0\(second_)"
        return (hour, minute,second)
    }
}
