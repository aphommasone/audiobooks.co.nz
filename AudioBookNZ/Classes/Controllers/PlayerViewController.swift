//
//  ViewController.swift
//  Music Player
//
//  Created by polat on 19/08/14.
//  Copyright (c) 2014 polat. All rights reserved.
// contact  bpolat@live.com

// Build 3 - July 1 2015 - Please refer git history for full changes
// Build 4 - Oct 24 2015 - Please refer git history for full changes

//Build 5 - Dec 14 - 2015 Adding shuffle - repeat
//Build 6 - Oct 10 - 2016 iOS 10 update - iPhone 3g, 3gs screensize no more supported.



import UIKit
import AVFoundation
import MediaPlayer

struct Chapter {
    var title:String
    var start:Int
    var duration:Int
    var index:Int
}


enum PlayerType {
    case MP3
    case M4B
}

class PlayerViewController: UIViewController,AVAudioPlayerDelegate {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    //Choose background here. Between 1 - 7
    let selectedBackground = 1
    
    var type: PlayerType = PlayerType.MP3
    
    var audioPlayer:AVAudioPlayer! = nil
    var currentAudio = ""
    var currentAudioPath:URL!
    var audioList:Array<AudioBookDownloads> = []
    var audiobookName:String = ""
    var audiobookImageSrc:String = ""
    var currentAudioIndex = 0
    var chapterList:[Chapter] = []
    var currentAudioChapter = 0
    var timer:Timer!
    var audioLength = 0.0
    var toggle = true
    var effectToggle = true
    var totalLengthOfAudio = ""
    var finalImage:UIImage!
    var isTableViewOnscreen = false
    var repeatState = false
    
    // Tinted image
    let playImage:UIImage = UIImage(named: "play")!.withRenderingMode(.alwaysTemplate)
    let pauseImage:UIImage = UIImage(named: "pause")!.withRenderingMode(.alwaysTemplate)
    let nextImage:UIImage = UIImage(named: "next_hard")!.withRenderingMode(.alwaysTemplate)
    let prevImage:UIImage = UIImage(named: "previous_hard")!.withRenderingMode(.alwaysTemplate)
    let jumpNextImage:UIImage = UIImage(named: "next")!.withRenderingMode(.alwaysTemplate)
    let jumpPrevImage:UIImage = UIImage(named: "previous")!.withRenderingMode(.alwaysTemplate)
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet var songNo : UILabel!
    @IBOutlet weak var albumArtworkImageView: UIImageView!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet var songNameLabel : UILabel!
    @IBOutlet var songNameLabelPlaceHolder : UILabel!
    @IBOutlet var progressTimerLabel : UILabel!
    @IBOutlet var playerProgressSlider : UISlider!
    @IBOutlet var totalLengthOfAudioLabel : UILabel!
    @IBOutlet var previousButton : UIButton!
    @IBOutlet var playButton : UIButton!
    @IBOutlet var nextButton : UIButton!
    
    
    @IBOutlet weak var jumpNextButton: UIButton!
    @IBOutlet weak var jumpPrevButton: UIButton!
    
    @IBOutlet var listButton : UIButton!
    @IBOutlet var blurImageView : UIImageView!
    @IBOutlet var enhancer : UIView!
    
    
    @IBOutlet weak var repeatButton: UIButton!
    
    @IBAction func closePlayer(_ sender: Any) {
        audioPlayer.stop();
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: .AVAudioSessionRouteChange, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
    //MARK:- Lockscreen Media Control
    
    // This shows media info on lock screen - used currently and perform controls
    func retrieveChaptersM4B()
    {
        if self.currentAudioPath != nil {
            
            self.chapterList = Helper.shared.retrieveChaptersM4B(asset: AVAsset(url: self.currentAudioPath))
        }
    }
    
    func showMediaInfo(){
        // TODO Arnaud : read from a model
        let artistName = self.audiobookName
        let currentDownload = self.audioList[currentAudioIndex]
        let songName = currentDownload.name
        MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyArtist : artistName,  MPMediaItemPropertyTitle : songName]
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event!.type == UIEventType.remoteControl{
            switch event!.subtype{
            case UIEventSubtype.remoteControlPlay:
                play(self)
            case UIEventSubtype.remoteControlPause:
                play(self)
            case UIEventSubtype.remoteControlNextTrack:
                next(self)
            case UIEventSubtype.remoteControlPreviousTrack:
                previous(self)
            default:
                print("There is an issue with the control")
            }
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.default
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //assing background
        backgroundImageView.backgroundColor = UIColor.white
        
        //LockScreen Media control registry
        if UIApplication.shared.responds(to: #selector(UIApplication.beginReceivingRemoteControlEvents)){
            UIApplication.shared.beginReceivingRemoteControlEvents()
            UIApplication.shared.beginBackgroundTask(expirationHandler: { () -> Void in
            })
        }
        // Notify change route
        self.setupNotifications()
    }
    
    func setupNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(handleRouteChange),
                                       name: .AVAudioSessionRouteChange,
                                       object: nil)
    }
    
    func handleRouteChange(notification: Notification) {
        guard let userInfo = notification.userInfo,
            let reasonValue = userInfo[AVAudioSessionRouteChangeReasonKey] as? UInt,
            let reason = AVAudioSessionRouteChangeReason(rawValue:reasonValue) else {
                return
        }
        switch reason {
        case .newDeviceAvailable, .oldDeviceUnavailable:
            self.pauseAudioPlayer()
            (playButton.setImage( pauseImage, for: UIControlState()))
            break
        // Handle old device removed.
        default: ()
        }
    }
    
    func setRepeatAndShuffle(){
        repeatState = UserDefaults.standard.bool(forKey: "repeatState")
        
        if repeatState == true {
            repeatButton.isSelected = true
        }else{
            repeatButton.isSelected = false
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
       checkTransitionOrientation()
    }
    
    func checkTransitionOrientation() {
        self.albumArtworkImageView.isHidden =  UIDevice.current.orientation.isLandscape
        if UIDevice.current.orientation.isLandscape
        {
            self.topConstraint.constant = -170
        }
        else
        {
            self.topConstraint.constant = 19
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        // Enable autolock
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Disable autolock
        UIApplication.shared.isIdleTimerDisabled = true

        // Hide next/back button if only one element
        switch type {
        case .MP3:
            if (self.audioList.count < 2) {
                self.nextButton.isHidden = true
                self.previousButton.isHidden = true
            }
            break
        case .M4B:
            self.nextButton.isHidden = false
            self.previousButton.isHidden = false
            break
        default:
            if (self.audioList.count < 2) {
                self.nextButton.isHidden = true
                self.previousButton.isHidden = true
            }
        }
        
        prepareAudio()
        updateLabels()
        assingSliderUI()
        setRepeatAndShuffle()
        retrievePlayerProgressSliderValue()
        // We need to get inf for M4B audiofiles
        retrieveChaptersM4B()
        // We recall the total song length
        showTotalSongLength()
        
        // Set tint
        self.playButton.setImage( playImage, for: UIControlState())
        self.nextButton.setImage( nextImage, for: UIControlState())
        self.previousButton.setImage( prevImage, for: UIControlState())
        self.jumpNextButton.setImage( jumpNextImage, for: UIControlState())
        self.jumpPrevButton.setImage( jumpPrevImage, for: UIControlState())
        
        self.playButton.tintColor = UIColor.black
        self.nextButton.tintColor = UIColor.black
        self.previousButton.tintColor = UIColor.black
        
        self.jumpNextButton.tintColor = UIColor.black
        self.jumpPrevButton.tintColor = UIColor.black
        
        self.albumArtworkImageView.isHidden =  UIDevice.current.orientation.isLandscape
        
        if self.type == .M4B {
            self.playFromChapter(chapter: self.currentAudioChapter)
        }
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK:- AVAudioPlayer Delegate's Callback method
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool){
        if flag == true {
            if repeatState == false {
                // Go to the next audio
                self.playNextAudio()
                
                return
            
            } else {
            //repeat same song
                prepareAudio()
                playAudio()
            
            }
        }
    }
    
    
    //Sets audio file URL
   
    
    func setCurrentAudioPath(){
        let download = self.audioList[currentAudioIndex]
        
        let documentURL = Helper.shared.getAudioPathURL(download: download)
        
        if documentURL != nil {
            currentAudio = download.name
            currentAudioPath = documentURL
        }
    }
    
    func saveCurrentTrackNumber(){
        UserDefaults.standard.set(currentAudioIndex, forKey:"currentAudioIndex")
        UserDefaults.standard.synchronize()
        
    }
    
    // Prepare audio for playing
    func prepareAudio(){
        setCurrentAudioPath()
        do {
            //keep alive audio at background
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            do {
            
            try AVAudioSession.sharedInstance().setActive(true)
            } catch _ {
            
            }
        } catch _ {
        
        }
        UIApplication.shared.beginReceivingRemoteControlEvents()
        audioPlayer = try? AVAudioPlayer(contentsOf: currentAudioPath)
        audioPlayer.delegate = self
        
        audioLength = audioPlayer.duration
        
        playerProgressSlider.maximumValue = CFloat(audioPlayer.duration)
        playerProgressSlider.minimumValue = 0.0
        playerProgressSlider.value = 0.0
        audioPlayer.prepareToPlay()
        showTotalSongLength()
        updateLabels()
        progressTimerLabel.text = "00:00"
        
        
    }
    
    //MARK:- Player Controls Methods
    func  playAudio(){
        audioPlayer.play()
        startTimer()
        updateLabels()
        saveCurrentTrackNumber()
        showMediaInfo()
    }
    
    func playNextAudio(){
        currentAudioIndex += 1
        if currentAudioIndex>audioList.count-1{
            currentAudioIndex -= 1
            playButton.setImage( playImage, for: UIControlState())
            pauseAudioPlayer()
            return
        }
        prepareAudio()
        playAudio()
    }
    
    func playNextChapter() {
        currentAudioChapter += 1
        if currentAudioChapter>chapterList.count-1{
            currentAudioChapter -= 1
            playButton.setImage( playImage, for: UIControlState())
            pauseAudioPlayer()
            return
        }
        
        let chapter = self.chapterList[currentAudioChapter]
        audioPlayer.currentTime = TimeInterval(chapter.start)
        print("we will be at chapter \(currentAudioChapter + 1) for Chapter \(chapter)")
    }
    
    func playFromChapter(chapter: Int) {
        currentAudioChapter = chapter
        playButton.setImage( playImage, for: UIControlState())
        pauseAudioPlayer()
        
        let chapter = self.chapterList[currentAudioChapter]
        
        if  UserDefaults.standard.object(forKey: chapter.title + "playerProgressSliderValue") != nil {
            // We will check if te value is stored somehwere for the current  chapter play position
            let currentChaptervalue =  UserDefaults.standard.float(forKey: chapter.title + "playerProgressSliderValue")
            audioPlayer.currentTime = TimeInterval(currentChaptervalue)
        }
        else {
            // If not we getting back to beginning
            audioPlayer.currentTime = TimeInterval(chapter.start)
        }
        let currentChaptervalue =  UserDefaults.standard.float(forKey: chapter.title + "playerProgressSliderValue")
        
        
        let time = Helper.shared.calculateTimeFromNSTimeInterval(audioPlayer.currentTime)
        progressTimerLabel.text  = "\(time.hour):\(time.minute):\(time.second)"
        playerProgressSlider.value = CFloat(audioPlayer.currentTime)
        
        let currentChapter = self.chapterList[self.currentAudioChapter]
        
        // Update the song name outlets
        self.songNameLabel.text = "\(currentChapter.title)"
        
        print("we will be at chapter \(currentAudioChapter + 1) for Chapter \(chapter)")
    }
    
    
    func playPrevChapter() {
        currentAudioChapter -= 1
        if currentAudioChapter<0{
            currentAudioChapter += 1
            pauseAudioPlayer()
            (playButton.setImage( pauseImage, for: UIControlState()))
            return
        }
        
        let chapter = self.chapterList[currentAudioChapter]
        
        audioPlayer.currentTime = TimeInterval(chapter.start)
        print("we will be at chapter \(currentAudioChapter + 1) for Chapter \(chapter)")
    }
    
    func playPreviousAudio(){
        currentAudioIndex -= 1
        if currentAudioIndex<0{
            currentAudioIndex += 1
            return
        }
        if audioPlayer.isPlaying{
            prepareAudio()
            playAudio()
        }else{
            prepareAudio()
        }
        
    }
    
    
    func stopAudiplayer(){
        audioPlayer.stop();
        
    }
    
    func pauseAudioPlayer(){
        audioPlayer.pause()
        
    }
    
    func findCurrentAudiobookChapterIndex(duration: Int) -> Int{
        var chapterIndex = 0
        for chapter in self.chapterList {
            if duration < (chapter.duration + chapter.start) {
                return chapterIndex
            }
            chapterIndex = chapterIndex + 1
        }
        return chapterIndex
    }
    
    func findCurrentAudiobookChapter(duration: Int) -> Chapter{
        for chapter in self.chapterList {
            if duration < (chapter.duration + chapter.start) {
                return chapter
            }
        }
        return self.chapterList.first!
    }
    
    //!MARK:-
    
    func startTimer(){
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PlayerViewController.update(_:)), userInfo: nil,repeats: true)
            timer.fire()
        }
    }
    
    func stopTimer(){
        timer.invalidate()
        
    }
    
    
    func update(_ timer: Timer){
        if !audioPlayer.isPlaying{
            return
        }
        let time = Helper.shared.calculateTimeFromNSTimeInterval(audioPlayer.currentTime)
        progressTimerLabel.text  = "\(time.hour):\(time.minute):\(time.second)"
        playerProgressSlider.value = CFloat(audioPlayer.currentTime)
        let currentDownload = self.audioList[currentAudioIndex]
        if (type == .M4B)
        {
            let currentChapterIndex = findCurrentAudiobookChapterIndex(duration: Int(audioPlayer.currentTime))
            print("current chapter Index : \(currentChapterIndex)")
            self.currentAudioChapter = currentChapterIndex
            
            let currentChapter = self.chapterList[self.currentAudioChapter]
            
            // Update the song name outlets
            self.songNameLabel.text = "\(currentChapter.title)"
            
            // UserDefaults.standard.float(forKey: self.chapterList[self.currentAudioChapter].title+"playerProgressSliderValue")
            UserDefaults.standard.set(playerProgressSlider.value , forKey: currentChapter.title + "playerProgressSliderValue")
            UserDefaults.standard.set(currentChapter.title , forKey: "currentM4BChapter")
        }
        UserDefaults.standard.set(playerProgressSlider.value , forKey: currentDownload.name + "playerProgressSliderValue")
       
    
        
    }
    
    func retrievePlayerProgressSliderValue(){
        let currentDownload = self.audioList[currentAudioIndex]
        
        var playerProgressSliderValue = UserDefaults.standard.float(forKey: currentDownload.name + "playerProgressSliderValue") - Constants.AudioPlayer.kPlayerThrowbackSeconds
        
        
        if playerProgressSliderValue != 0 {
            playerProgressSlider.value  = playerProgressSliderValue
            audioPlayer.currentTime = TimeInterval(playerProgressSliderValue)
            
            let time = Helper.shared.calculateTimeFromNSTimeInterval(audioPlayer.currentTime)
            progressTimerLabel.text  = "\(time.hour):\(time.minute):\(time.second)"
            playerProgressSlider.value = CFloat(audioPlayer.currentTime)
            
        }else{
            playerProgressSlider.value = 0.0
            audioPlayer.currentTime = 0.0
            progressTimerLabel.text = "00:00:00"
        }
    }


    
    func showTotalSongLength(){
        switch type {
        case .M4B:
            calculateAudiobooksLength()
            break
        case .MP3:
            calculateSongLength()
            break
        default:
            break
        }
        totalLengthOfAudioLabel.text = totalLengthOfAudio
    }
    
    func calculateSongLength(){
        let time = Helper.shared.calculateTimeFromNSTimeInterval(audioLength)
        totalLengthOfAudio = "\(time.minute):\(time.second)"
    }
    
    func calculateAudiobooksLength() {
        var audioLength = 0
        if self.chapterList.count > 0
        {
            let lastChapter = self.chapterList.last
            let start = lastChapter?.start as! Int
            let duration = lastChapter?.duration as! Int
            audioLength = start + duration
            let time = Helper.shared.calculateTimeFromNSTimeInterval(TimeInterval(audioLength))
            totalLengthOfAudio = "\(time.hour):\(time.minute):\(time.second)"
        }
        else{
            totalLengthOfAudio = "00:00"
        }
        
    }
    
    func updateLabels(){
        updateArtistNameLabel()
        updateSongNameLabel()
        updateAlbumArtwork()
    }
    
    
    func updateAlbumArtwork(){
        self.albumArtworkImageView.af_setImage(withURL: URL.init(string: self.audiobookImageSrc)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
    }
    
    func updateArtistNameLabel(){
        self.artistNameLabel.text = self.audiobookName
    }
    
    func updateSongNameLabel(){
        let currentDownload = self.audioList[currentAudioIndex]
        self.songNameLabel.text = currentDownload.name
    }
    
    func assingSliderUI () {
        let minImage = UIImage(named: "slider-track-fill")
        let maxImage = UIImage(named: "slider-track")
        let thumb = UIImage(named: "thumb")
 
        playerProgressSlider.setMinimumTrackImage(minImage, for: UIControlState())
        playerProgressSlider.setMaximumTrackImage(maxImage, for: UIControlState())
        playerProgressSlider.setThumbImage(thumb, for: UIControlState())
    }
    
    @IBAction func play(_ sender : AnyObject) {
        
        if audioPlayer.isPlaying{
            pauseAudioPlayer()
            audioPlayer.isPlaying ? "\(playButton.setImage( pauseImage, for: UIControlState()))" : "\(playButton.setImage(playImage , for: UIControlState()))"
        }else{
            playAudio()
            audioPlayer.isPlaying ? "\(playButton.setImage( pauseImage, for: UIControlState()))" : "\(playButton.setImage(playImage , for: UIControlState()))"
        }
    }
    
    @IBAction func next(_ sender : AnyObject) {
        switch type {
        case .MP3:
            playNextAudio()
            break
        case .M4B:
            playNextChapter()
            break
        default: break
        }
    }
    
    func jumpAudio(seconds: Double) {
        let currentTime = seconds
        let time = Helper.shared.calculateTimeFromNSTimeInterval(currentTime)
        progressTimerLabel.text  = "\(time.hour):\(time.minute):\(time.second)"
        playerProgressSlider.value = CFloat(currentTime)
        
        self.audioPlayer.currentTime = currentTime
        
        // Need to save in the preferences
        let currentDownload = self.audioList[currentAudioIndex]
        let currentChapter = self.chapterList[currentAudioChapter]
        UserDefaults.standard.set(playerProgressSlider.value , forKey: currentDownload.name + "playerProgressSliderValue")
        UserDefaults.standard.set(playerProgressSlider.value , forKey: currentChapter.title + "playerProgressSliderValue")
        UserDefaults.standard.set(currentChapter.title , forKey: "currentM4BChapter")
    }
    
    @IBAction func nextOneMinute(_ sender: Any) {
        self.jumpAudio(seconds: audioPlayer.currentTime + Double(Constants.AudioPlayer.kPlayerJumpNextSeconds))
    }
    
    @IBAction func prevOneMinute(_ sender: Any) {
        self.jumpAudio(seconds: audioPlayer.currentTime + Double(Constants.AudioPlayer.kPlayerJumpPrevSeconds))
    }
    
    @IBAction func previous(_ sender : AnyObject) {
        switch type {
        case .MP3:
            playPreviousAudio()
            break
        case .M4B:
            playPrevChapter()
            break
        default: break
        }
    }
    
    @IBAction func changeAudioLocationSlider(_ sender : UISlider) {
        audioPlayer.currentTime = TimeInterval(sender.value)
    }
    
    @IBAction func userTapped(_ sender : UITapGestureRecognizer) {
        play(self)
    }
    
    @IBAction func userSwipeLeft(_ sender : UISwipeGestureRecognizer) {
        next(self)
    }
    
    @IBAction func userSwipeRight(_ sender : UISwipeGestureRecognizer) {
        previous(self)
    }
    
    @IBAction func repeatButtonTapped(_ sender: UIButton) {
        if sender.isSelected == true {
            sender.isSelected = false
            repeatState = false
            UserDefaults.standard.set(false, forKey: "repeatState")
        } else {
            sender.isSelected = true
            repeatState = true
            UserDefaults.standard.set(true, forKey: "repeatState")
        }
    }
}
