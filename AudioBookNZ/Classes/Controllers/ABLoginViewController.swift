//
//  LoginViewController.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import UIKit
import SafariServices

class ABLoginViewController: UIViewController {
    
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var usernameTextfield: UITextField!
    
    // Constraints
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var loginForgotPasswordViewBottomConstraint: NSLayoutConstraint!
    
    let logoTopConstraintPortrait: CGFloat = 77
    @IBOutlet weak var removeCredentialsButton: UIButton!
    
    @IBOutlet weak var registerButton: UIButton!
    let logoTopConstraintLandscape: CGFloat = 15
    
    let loginForgotPasswordViewBottomConstraintPortrait: CGFloat = 48
    let loginForgotPasswordViewBottomConstraintLandscape: CGFloat = 0
    
    let loginPresenter = AuthentificationPresenter(authentificationService: ABAuthentificationService())

    var preventAutoLoggingIn = false
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // We need to attach the presenter here
        self.loginPresenter.attachView(view: self)
       
        self.usernameTextfield.delegate = self
        self.passwordTextfield.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        let credentials: (username: String, password: String) = self.loginPresenter.getStoredCredentials()!
        if (credentials.username=="" && credentials.password=="") {
            setUIEmptyCredentials()
        }
        else
        {
            setUICredentials(username: credentials.username, password: credentials.password)
            
            if preventAutoLoggingIn == false {
                if (credentials.username == Constants.Demo.kDemoCredentialsUsername && credentials.password == Constants.Demo.kDemoCredentialsPassword) {
                }
                else {
                    self.loginPresenter.getUserCredentials(username: credentials.username, password: credentials.password)
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            logoTopConstraint.constant = 15
            loginForgotPasswordViewBottomConstraint.constant = 48
        }
        else {
            logoTopConstraint.constant = 77
            loginForgotPasswordViewBottomConstraint.constant = 48

        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: Observer
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect {
            if UIDevice.current.orientation.isPortrait {
                logoTopConstraint.constant = logoTopConstraintPortrait - keyboardSize.height
                loginForgotPasswordViewBottomConstraint.constant = loginForgotPasswordViewBottomConstraintPortrait + keyboardSize.height
            }
            else {
                logoTopConstraint.constant = logoTopConstraintLandscape - keyboardSize.height
                loginForgotPasswordViewBottomConstraint.constant = loginForgotPasswordViewBottomConstraintLandscape + keyboardSize.height
            }
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if UIDevice.current.orientation.isPortrait {
            logoTopConstraint.constant = logoTopConstraintPortrait
            loginForgotPasswordViewBottomConstraint.constant = loginForgotPasswordViewBottomConstraintPortrait
        }
        else {
            logoTopConstraint.constant = logoTopConstraintLandscape
            loginForgotPasswordViewBottomConstraint.constant = loginForgotPasswordViewBottomConstraintLandscape
        }
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: Actions
    
    @IBAction func emptyCredentials(_ sender: Any) {
        self.loginPresenter.emptyStoredCredentials()
        self.setUIEmptyCredentials()
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        let safariVC = SFSafariViewController(url: URL(string: Constants.Network.kWebsiteForgotPasswordURL)!)
        self.present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
    
    @IBAction func registerNewAccountAction(_ sender: Any) {
        // Add safari view controller modal view
        
        let safariVC = SFSafariViewController(url: URL(string: Constants.Network.kWebsiteRegisterURL)!)
        self.present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
    
    @IBAction func loginAction(_ sender: Any) {
        let username = self.usernameTextfield.text
        let password = self.passwordTextfield.text
        
        // Test user 1, No product
//        let username = "theo_gibson"
//        let password = "Testaccount17"
        
//        let username = "testuser2@audiobooksnz.co.nz"
//            let password = "nAd$knslquC5"
        
        self.usernameTextfield.resignFirstResponder()
        self.passwordTextfield.resignFirstResponder()
        
        self.loginPresenter.getUserCredentials(username: username!, password: password!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginToAudiobookSegue" {
            let audiobooksListVC = segue.destination as! ABOrderListViewController
            audiobooksListVC.user = sender as! User
        }
    }
    
    // For setting up an empty credentials and a prefilled one
    func setUIEmptyCredentials() {
        self.usernameTextfield.text = ""
        self.passwordTextfield.text = ""
        self.usernameTextfield.isEnabled = true
        self.passwordTextfield.isHidden = false
        self.removeCredentialsButton.isHidden = true
        self.registerButton.isHidden = false
        
    }
    
    func setUICredentials(username: String, password: String) {
        self.usernameTextfield.text = username
        self.usernameTextfield.isEnabled = false
        self.passwordTextfield.text = password
        self.passwordTextfield.isHidden = true
        self.removeCredentialsButton.isHidden = false
        self.registerButton.isHidden = true
    }
    
}

extension ABLoginViewController : SFSafariViewControllerDelegate {
    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension ABLoginViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}

extension ABLoginViewController : ABAuthentificationView {
    func credentialsDidSucceeded(user: User) {
        self.performSegue(withIdentifier: "loginToAudiobookSegue", sender: user)
    }
    
    func credentialsDidFail() {
        // Blank the textfields
        self.usernameTextfield.text = ""
        self.passwordTextfield.text = ""
        
        // Show some alert saying credentials went wrong
        let failedAlert = UIAlertController(title: Constants.AlertPrompt.kInvalidCredentialsTitle, message: Constants.AlertPrompt.kInvalidCredentialsMessage, preferredStyle: .alert)
        failedAlert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(failedAlert, animated: true, completion: nil)
    }
}
