//
//  TwitterViewController.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/01/18.
//  Copyright © 2018 Arnaud. All rights reserved.
//

import UIKit

class TwitterViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var twitterWebview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        self.twitterWebview.delegate = self
        let twitterWebpageURL = URL(string:  Constants.Network.kTwitterURL);
        let myRequest = URLRequest(url: twitterWebpageURL!);
        self.twitterWebview.loadRequest(myRequest)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.twitterWebview.delegate = self
    }
    
    @IBAction func closeWindow(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        LoadingOverlay.shared.showOverlay()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingOverlay.shared.hideOverlayView()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
