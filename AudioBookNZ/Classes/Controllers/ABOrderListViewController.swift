//
//  ABOrderListViewController.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/05/17.
//  Copyright 2017 Arnaud. All rights reserved.
//

import UIKit
import KDLoadingView
import Alamofire
import AlamofireImage
import MessageUI

enum ToolbarStatus {
    case Closed
    case Opened
}

class ABOrderListViewController: UIViewController {

    // Bottom view
    var toolbarStatus: ToolbarStatus = .Closed
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var arrowBottomButton: UIButton!
    @IBOutlet weak var viewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    // Current main view
    @IBOutlet weak var noAudiobooksMessageTextView: UITextView!
    @IBOutlet weak var ordersTableView: UITableView!
    
    private let orderListPresenter = OrderListPresenter(audioBookService: ABAudiobookService())
    
    var user: User? = nil
    var orderListToDisplay = [AudioBook]()
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // We need to attach the presenter here
        self.orderListPresenter.attachView(view: self)
        
        self.customizeViewController()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.toggleBottom(opened: false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.orderListPresenter.notifViewWillDisappear(_from: self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // If we have no value by default, we will retrieve all the audiobooks. Otherwise let's wait for the hard refresh call
        if (self.orderListToDisplay.count < 1)
        {
            print("ABOrderListViewController > Get audiobooks for \(user?.name) (\(user?.userId)")
            self.orderListPresenter.getAudioBooks(userId: (user?.userId)!)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "audiobookToDownloadSegue" {
            let downloadsVC = segue.destination as! ABDownloadsViewController
            downloadsVC.audiobook = self.orderListToDisplay[(self.ordersTableView.indexPathForSelectedRow?.row)!]
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    @IBAction func twitterShareAction(_ sender: Any) {
        self.orderListPresenter.goTwitter()
    }
    
    @IBAction func emailShareAction(_ sender: Any) {
        self.orderListPresenter.goEmail()
    }
    
    @IBAction func facebookShareAction(_ sender: Any) {
        self.orderListPresenter.goFacebook()
    }
    
    @IBAction func openingBottomAction(_ sender: Any) {
        var opened = false
        if (self.toolbarStatus == .Closed)
        {
            self.toolbarStatus = .Opened
            opened = true
        }
        else
        {
            self.toolbarStatus = .Closed
            opened = false
        }
        
        self.toggleBottom(opened: opened, animated: true)
    }
    
    
    func customizeViewController() {
        // Need to hide back button to the login page (create a logout button instead)
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isHidden = false
        
        // Customize the text colour of the navigation bar
        self.navigationController?.navigationBar.tintColor = UIColor(red: (255/255.0), green: (255/255.0), blue: (255/255.0), alpha: 1.0)
        
        // Add buttons logout on the left of the navbar
        let logoutButton = UIButton.init(type: .custom)
        let logoutImage = UIImage.init(named: "logout-50.png")
        let logoutImageUp = UIImage.init(named: "logout-up-50.png")
        
        logoutButton.setImage(resizeImage(image: logoutImage!, newWidth: 30), for: UIControlState.normal)
        logoutButton.setImage(resizeImage(image: logoutImageUp!, newWidth: 30), for: UIControlState.highlighted)
        logoutButton.addTarget(self, action:#selector(self.logoutAction), for: UIControlEvents.touchUpInside)
        logoutButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let logoutBarButton = UIBarButtonItem.init(customView: logoutButton)
        self.navigationItem.leftBarButtonItem = logoutBarButton
        
        // Add buttons logout on the left of the navbar
        
        let refreshImage = UIImage.init(named: "refresh-50.png")
        let refreshImageUp = UIImage.init(named: "refresh-up-50.png")
        
        let resfreshButton = UIButton.init(type: .custom)
        resfreshButton.setImage(resizeImage(image: refreshImage!, newWidth: 30), for: UIControlState.normal)
        resfreshButton.setImage(resizeImage(image: refreshImageUp!, newWidth: 30), for: UIControlState.highlighted)
        resfreshButton.addTarget(self, action:#selector(self.refreshAction), for: UIControlEvents.touchUpInside)
        resfreshButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let resfreshBarButtonBar = UIBarButtonItem.init(customView: resfreshButton)
        self.navigationItem.rightBarButtonItem = resfreshBarButtonBar
    }
    
    func logoutAction(_ sender: Any) {
        
        // Show some alert saying credentials went wrong
        let logoutAlert = UIAlertController(title: Constants.AlertPrompt.kLogoutWarningTitle, message: Constants.AlertPrompt.kLogoutWarningMessage, preferredStyle: .alert)
        logoutAlert.addAction(UIAlertAction.init(title: "Yes", style: UIAlertActionStyle.default, handler: { (alertAction) in
            self.orderListPresenter.logout()
            self.dismiss(animated: true, completion: nil)
        }))
        logoutAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel, handler:nil))
        
        self.present(logoutAlert, animated: true, completion: nil)
        
    }
    
    func refreshAction(_ sender: Any) {
        self.orderListPresenter.getAudioBooks(userId: (user?.userId)!)
    }
}

// MARK: - TableView Delegate & Datasource
extension ABOrderListViewController: UITableViewDelegate,UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orderListToDisplay.count;
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListTableViewCell", for:indexPath) as! ProductListTableViewCell
        
        let audiobook = self.orderListToDisplay[indexPath.row]
        cell.productTitleLabel.text = audiobook.title
//        cell.productCategorieLabel.text = audiobook.categories.joined(separator: ", ")
        
        cell.productImageView.af_setImage(withURL: URL.init(string: audiobook.imageSrc!)!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "audiobookToDownloadSegue", sender: self)
    }

}

extension ABOrderListViewController: ABOrderListView {
    func loadTableView(audiobook: [AudioBook]) {
        orderListToDisplay = audiobook
        ordersTableView.reloadData()
        
        if (audiobook.count > 0) {
            self.ordersTableView.isHidden = false
            self.noAudiobooksMessageTextView.isHidden = true
        }
        else
        {
            //Set the formatting link manually
            let linkAttributes = [
                NSLinkAttributeName: NSURL(string: Constants.Network.kWebsiteURL),
                NSForegroundColorAttributeName: UIColor.blue
                ] as [String: Any]
            
            let attributedString = NSMutableAttributedString(string: Constants.Text.kTextNoProductsAvailable)
            //Set the "click here" substring to the link
            attributedString.setAttributes(linkAttributes, range: NSMakeRange(95, 21))
            self.noAudiobooksMessageTextView.attributedText = attributedString
            self.noAudiobooksMessageTextView.font = UIFont(name:"Poppins-Light", size: 17)
            
            self.ordersTableView.isHidden = true
            self.noAudiobooksMessageTextView.isHidden = false
        }
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func createdMailComposer(mail: MFMailComposeViewController)
    {
        self.present(mail, animated: true, completion: nil)
    }
}


extension ABOrderListViewController
{
    func toggleBottom(opened: Bool, animated: Bool)
    {
        
        if (opened)
        {
            self.viewBottomConstraint.constant = CGFloat(Constants.Animation.kBottomOpenedConstraintValue)
            
            UIView.animate(withDuration: (!animated) ? 0 :  Constants.Animation.kBottomButtonTimespeed, animations: {
                self.arrowBottomButton.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi))
                self.view.layoutIfNeeded()
            })
        }
        else
        {
            self.viewBottomConstraint.constant = CGFloat(Constants.Animation.kBottomClosedConstraintValue)
            
            UIView.animate(withDuration:(!animated) ? 0 :  Constants.Animation.kBottomButtonTimespeed, animations: {
                self.arrowBottomButton.transform = CGAffineTransform.identity
                self.view.layoutIfNeeded()
            })
        }
        
    }
}
