//
//  ABDownloadsViewController.swift
//  AudioBookNZ
//
//  Created by aphommasone on 27/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import ObjectMapper
import Realm
import RealmSwift

class ABDownloadsViewController: UIViewController {
    var audiobook: AudioBook? = nil
    
    @IBOutlet weak var audiobookDescriptionHtmlLabel: UILabel!
    @IBOutlet weak var audiobookTitleLabel: UILabel!
    @IBOutlet weak var audiobookImageView: UIImageView!
    
    @IBOutlet weak var audiobookTableView: UITableView!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var formatSegmented: UISegmentedControl!
    
    @IBOutlet weak var headerDownloadsTableview: UIView!
    public let downloadListPresenter = DownloadListPresenter(downloadService: ABDownloadService())
    
    public var tableviewDownloadsDatasource: List<AudioBookDownloads> = List()
    // This is for m4b chapteres only, we will not display tableviewDownloadsDatasource
    // for them but we will keep AudiobooksDownload so we could transmit to audioplayers
    public var tableViewChaptersDatasource: [[Chapter]] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.downloadListPresenter.attachView(view: self)
        
        customizeViewController()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func customizeViewController() {
        // Add buttons logout on the left of the navbar
        let resfreshButton = UIButton.init(type: .custom)
        let imageUninstall = UIImage.init(named: "uninstall.png")
        let imageUninstallUp = UIImage.init(named: "uninstall-filled.png")
        
        resfreshButton.setImage(resizeImage(image: imageUninstall!, newWidth: 30), for: UIControlState.normal)
        resfreshButton.setImage(resizeImage(image: imageUninstallUp!, newWidth: 30), for: UIControlState.highlighted)
        resfreshButton.addTarget(self, action:#selector(self.removeDowloads), for: UIControlEvents.touchUpInside)
        resfreshButton.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let resfreshBarButtonBar = UIBarButtonItem.init(customView: resfreshButton)
        self.navigationItem.rightBarButtonItem = resfreshBarButtonBar
    }
    
    func removeDowloads(_ sender: Any) {
        // Show confirmation first before downloading all downloads
        let failedAlert = UIAlertController(title: Constants.AlertPrompt.kRemoveDownloadsWarningTitle, message: Constants.AlertPrompt.kRemoveDownloadsMessage, preferredStyle: .alert)
        failedAlert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler: { (alertAction) in
            // call remove download functions
            self.downloadListPresenter.removeAllDownloads(audiobook: self.audiobook!)
            self.audiobookTableView.reloadData()
            self.dismiss(animated: true, completion: nil)
        }))
        failedAlert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.destructive, handler: { (alertAction) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(failedAlert, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableViewChaptersDatasource = []
        self.audiobookTableView.reloadData()
        
        self.audiobookTitleLabel.text = audiobook?.title
        self.audiobookImageView.af_setImage(withURL: URL(string: (audiobook?.imageSrc)!)!)
        
        if audiobook?.type == "" {
            self.formatSegmented.selectedSegmentIndex = 0
            self.formatSegmented.isHidden = false
            var newHeaderFrame = self.headerDownloadsTableview.frame
            newHeaderFrame.size.height = 128
            self.headerDownloadsTableview.frame = newHeaderFrame
            
            // We grab mp3 by default
            self.tableviewDownloadsDatasource = getDownloadsByFormat(format: "MP3")
        }
        else {
            // We dont want to take all files, just the one specified in type
            self.tableviewDownloadsDatasource = getDownloadsByFormat(format: (audiobook?.type)!)
            
            // Need to add the chapters
            if audiobook?.type == "m4b" {
                self.tableViewChaptersDatasource = self.downloadListPresenter.getChaptersFromDownloads(downloads: Array(self.tableviewDownloadsDatasource))
            }
            
            self.formatSegmented.isHidden = true
            var newHeaderFrame = self.headerDownloadsTableview.frame
            newHeaderFrame.size.height = 60
            self.headerDownloadsTableview.frame = newHeaderFrame
        }
        
        
        self.audiobookTableView.reloadData()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // This is to avoid ot have a file named ".mp3". It woudl be better on that case to just format "title of the book" + "part" + "number"
    func formatFilenameIfNeeded(file: String, index: Int) -> String {
        if file.contains(".mp3") || file.contains(".m4b") || file.contains(".m4a") {
            // We need to format
            return String.init(format: "%@ - Part %i", (self.audiobook?.title)!, index)
        }
        return file
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "downloadToPlayerSegue" {
            let playerModalVC = segue.destination as! PlayerViewController
//            let downloads = self.audiobook?.downloads
            let downloads = self.tableviewDownloadsDatasource
            
            var selectedIndex = -1
            var currentChapter = 0
            
            if (self.tableViewChaptersDatasource.count > 0)
            {
                selectedIndex = (self.audiobookTableView.indexPathForSelectedRow?.section)!
                currentChapter = (self.audiobookTableView.indexPathForSelectedRow?.row)!
                
                // Will notifiy it's a favourite for now
                // NOt best logic for now
                let selectedCell = self.audiobookTableView.cellForRow(at:self.audiobookTableView.indexPathForSelectedRow!) as! ABDownloadTableViewCell
                UserDefaults.standard.set(selectedCell.nameLabel.text , forKey: "currentM4BChapter")
            }
            else {
                selectedIndex = (self.audiobookTableView.indexPathForSelectedRow?.row)!
            }
            playerModalVC.currentAudioChapter = currentChapter
            playerModalVC.currentAudioIndex = selectedIndex
            playerModalVC.audioList = Array(downloads)
            playerModalVC.audiobookName = (self.audiobook?.title)!
            playerModalVC.audiobookImageSrc = (self.audiobook?.imageSrc)!
            
            // Check if everythign was downloaded. If not just hide next/prev button
            if (self.isEverythingDownloaded() == false)
            {
                // Just pass one
                playerModalVC.currentAudioIndex = 0
                let tempsDownloads =  Array(self.tableviewDownloadsDatasource)
                let selectedDownloadable = tempsDownloads[selectedIndex]
                playerModalVC.audioList = [selectedDownloadable]
            }
            
            // Pass the type
            if self.formatSegmented.isHidden == false
            {
                if self.formatSegmented.selectedSegmentIndex == 1 {
                    playerModalVC.type = .M4B
                }
                else
                {
                    playerModalVC.type = .MP3
                }
            }
            else
            {
                if self.audiobook?.type.lowercased() == "m4b" {
                    playerModalVC.type = .M4B
                }
                else
                {
                    playerModalVC.type = .MP3
                }
            }
        }
    }
    
    @IBAction func formatSelectionValueChanged(_ sender: Any) {
        let segment = sender as! UISegmentedControl
        // MP3
        if segment.selectedSegmentIndex == 0 {
            self.tableviewDownloadsDatasource = getDownloadsByFormat(format: "MP3")
            
            // Empty it
            self.tableViewChaptersDatasource = []
        }
        // M4B
        else {
            self.tableviewDownloadsDatasource = getDownloadsByFormat(format: "M4B")
            
            // Add the chapters
            self.tableViewChaptersDatasource = self.downloadListPresenter.getChaptersFromDownloads(downloads: Array(self.tableviewDownloadsDatasource))
        }
        self.audiobookTableView.reloadData()
    }
    
    
    
    func isEverythingDownloaded() -> Bool {
        for download in self.tableviewDownloadsDatasource {
            if (self.downloadListPresenter.downloadAvailable(download: download) == false) {
                return false
            }
        }
        return true
    }
    
    func getDownloadsByFormat(format: String) -> List<AudioBookDownloads> {
        var newDownloadsFormat: List<AudioBookDownloads> = List()
        let formattedString = ".\(format.lowercased())"
        for download in (self.audiobook?.downloads)! {
            if download.file.contains(formattedString) {
                newDownloadsFormat.append(download)
            }
        }
        return newDownloadsFormat
    }
}


// MARK: - TableView Delegate & Datasource
extension ABDownloadsViewController: UITableViewDelegate,UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.tableViewChaptersDatasource.count > 0) {
            return self.tableViewChaptersDatasource[section].count
        }
        return tableviewDownloadsDatasource.count
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if (self.tableViewChaptersDatasource.count > 0) {
            return self.tableviewDownloadsDatasource.count
        }
        return 1
    }
    
    // Add a section header
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.tableViewChaptersDatasource.count > 0 {
            let download = tableviewDownloadsDatasource[section]
            return download.name
        }
        return nil
    }
    
    public func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        vw.backgroundColor = UIColor(red: (209/255.0), green: (48/255.0), blue: (71/255.0), alpha: 1.0)
        return vw
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "downloadTableViewCell", for:indexPath) as! ABDownloadTableViewCell
        
        if self.tableViewChaptersDatasource.count > 0 {
            let download = tableviewDownloadsDatasource[indexPath.section]
            let chapter = tableViewChaptersDatasource[indexPath.section][indexPath.row]
            cell.nameLabel.text = chapter.title
        
            if chapter.title == UserDefaults.standard.string(forKey: "currentM4BChapter") {
                cell.statusReadingChapterLabel.isHidden = false
                
                // "10 DTOTWCYLF Chapter 09playerProgressSliderValue"
                let savedTime =  UserDefaults.standard.float(forKey: chapter.title + "playerProgressSliderValue")
                // We need to check if it's not 0. If it's the case well we need to indicated the beginning of the chapter
                let time = Helper.shared.calculateTimeFromNSTimeInterval(TimeInterval(savedTime))
                cell.statusReadingChapterLabel.text = "Currently at \(time.hour):\(time.minute):\(time.second)"
                
            }
            else {
                cell.statusReadingChapterLabel.isHidden = true
            }
            
            
            // Set the status on the cell
            if (self.downloadListPresenter.downloadAvailable(download: download) ) {
                // Append the size of the downloads
                let size = self.downloadListPresenter.getDownloadSize(download: download)
//                cell.nameLabel.text = formattedFileNameIfNeeded + "(\(size))"
                cell.updateUI(forState: .Downloaded)
            }
            else {
                cell.updateUI(forState: .NotDownloaded)
            }
        }
        else
        {
            let download = tableviewDownloadsDatasource[indexPath.row]
            let formattedFileNameIfNeeded = formatFilenameIfNeeded(file: download.name, index: indexPath.row+1)
            cell.nameLabel.text = formattedFileNameIfNeeded
            
            // Set the status on the cell
            if (self.downloadListPresenter.downloadAvailable(download: download) ) {
                // Append the size of the downloads
                let size = self.downloadListPresenter.getDownloadSize(download: download)
                cell.nameLabel.text = formattedFileNameIfNeeded + "(\(size))"
                cell.updateUI(forState: .Downloaded)
            }
            else {
                cell.updateUI(forState: .NotDownloaded)
            }
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var rightDownloadIndex = indexPath.row
        // It means we have chapters there
        if self.tableViewChaptersDatasource.count > 0 {
            rightDownloadIndex = indexPath.section
        }
        let download = self.tableviewDownloadsDatasource[rightDownloadIndex]
        let cell: ABDownloadTableViewCell = tableView.cellForRow(at: indexPath) as! ABDownloadTableViewCell
        self.downloadListPresenter.download(audiobook: download, cell: cell)
    }
    
    // Preferencse
    func updateUserPreferencesCurrentChapter(_with value: Float) {
        
    }
}



extension ABDownloadsViewController : ABDownloadListView {
    func prepareAudiobookPlayer(){
        self.performSegue(withIdentifier: "downloadToPlayerSegue", sender: self)
    }
    
    func enableFormatSelection(enabled: Bool){
        self.formatSegmented.isUserInteractionEnabled = enabled
        if enabled {
            self.formatSegmented.tintColor = UIColor.init(colorLiteralRed: 209/255, green: 48/255, blue: 71/255, alpha: 1)
        }
        else {
            self.formatSegmented.tintColor = UIColor.gray
        }
    }
}
