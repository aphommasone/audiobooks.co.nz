//
//  ABUserModel.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import Realm
import RealmSwift

class User: Object, Mappable {
    dynamic var userId: Int = 0
    dynamic var name: String = ""
    dynamic var email: String = ""
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        userId <- map["result.userID"]
        name <- map["result.username"]
        email <- map["result.email"]
    }
}
