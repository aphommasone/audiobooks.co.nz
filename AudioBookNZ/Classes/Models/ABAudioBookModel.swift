//
//  ProductMapper.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 14/05/17.
//  Copyright 2017 Arnaud. All rights reserved.
//

import Foundation
import ObjectMapper
import Realm
import RealmSwift

class AudioBookDownloads: Object, Mappable {
    dynamic var id: String = ""
    dynamic var name: String = ""
    dynamic var file: String = ""
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        file <- map["file"]
    }
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func typeAudio() -> String{
        let fileUrl = URL(string: file)
        let fileType = fileUrl?.pathExtension
        
        return fileType!
    }
    
    func pathInDocuments() -> URL {
        let downloadName = name
        var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        documentsURL.appendPathComponent(downloadName)
        
        if (typeAudio() == "m4b") {
            documentsURL.appendPathExtension("m4a")
        }
        else {
            documentsURL.appendPathExtension(typeAudio())
        }
        return documentsURL
    }
}

class AudioBookCategory: Object {
    dynamic var name: String = ""
    required convenience init?(map: Map){
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "name"
    }
}

class AudioBook: Object, Mappable {
    dynamic var id = 0
    dynamic var title: String = ""
    dynamic var descriptionHtml: String? = ""
    // We only grab the first image
    dynamic var imageSrc: String? = ""
    var downloads: List<AudioBookDownloads> = List()
    dynamic var userId = 0
    dynamic var type: String = ""
 
    required convenience init?(map: Map){
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func mapping(map: Map) {
        id <- map["product.id"]
        title <- map["product.title"]
        descriptionHtml <- map["product.description"]
        imageSrc <- map["product.images.0.src"]
//        type <- map["product.meta.0.value"]
        downloads <- (map["product.downloads"], ArrayTransform<AudioBookDownloads>())
    }
}

//:MARK Array object mapping
class ArrayTransform<T:RealmSwift.Object> : TransformType where T:Mappable {
    typealias Object = List<T>
    typealias JSON = Array<AnyObject>
    
    let mapper = Mapper<T>()
    
    func transformFromJSON(_ value: Any?) -> List<T>? {
        let result = List<T>()
        if let tempArr = value as! Array<AnyObject>? {
            for entry in tempArr {
                let mapper = Mapper<T>()
                let model : T = mapper.map(JSON: entry as! [String : Any])!
                result.append(model)
            }
        }
        return result
    }
    
    func transformToJSON(_ value: Object?) -> JSON? {
        var results = [AnyObject]()
        if let value = value {
            for obj in value {
                let json = mapper.toJSON(obj)
                results.append(json as AnyObject)
            }
        }
        return results
    }
}

extension Results {
    
    func toArray () -> [Object] {
        var array = [Object]()
        for result in self {
            array.append(result)
        }
        return array
    }
}
