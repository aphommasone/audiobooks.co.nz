//
//  ABClientAPI.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire

/* Testing requests :
 http://localhost:8888/wordpress/wp-json/audiobooknz-api/v1/authenticate?username=arnaud&password=Welcome1
 */

// Get list of all order for client current ID
struct GetOrdersListURLRequestProvider: URLRequestConvertible {
    let idNumber: Int
    
    init(idNumber: Int) {
        self.idNumber = idNumber
    }
    
    func asURLRequest() throws -> URLRequest {
        return URLRequest(url: URL(string: "https://audiobooksnz.co.nz/wc-api/v2/posts/\(idNumber)")!)
    }
}

// Get list of all downloads for product ID
struct GetAudioDownloadsListURLRequestProvider: URLRequestConvertible {
    let idNumber: Int
    
    init(idNumber: Int) {
        self.idNumber = idNumber
    }
    
    func asURLRequest() throws -> URLRequest {
        return URLRequest(url: URL(string: "https://audiobooksnz.co.nz/wc-api/v2/posts/\(idNumber)")!)
    }
}

//
public class ABClientAPI {
}
