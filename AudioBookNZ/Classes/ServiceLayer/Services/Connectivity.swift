//
//  Connectivity.swift
//  AudioBookNZ
//
//  Created by Joel Lim on 27/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire

extension UIApplication {
    
    static func topViewController(base: UIViewController? = UIApplication.shared.delegate?.window??.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }

        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}

class Connectivity {
    
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    static func handleNoInternetConnection() {
        let alert = UIAlertController(title: Constants.AlertPrompt.kNoNetworkConnectivityTitle, message: Constants.AlertPrompt.kNoNetworkConnectivityMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
}

