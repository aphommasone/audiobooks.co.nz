//
//  ABDownloadService.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 7/06/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire


class ABDownloadService : NSObject {
    override init() {
    }
    
    func downloadFile(atUrl: String) {
        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        Alamofire.download(
            atUrl,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                //progress closure
            }).response(completionHandler: { (DefaultDownloadResponse) in
                //here you able to access the DefaultDownloadResponse
                //result closure
            })
    }
}
