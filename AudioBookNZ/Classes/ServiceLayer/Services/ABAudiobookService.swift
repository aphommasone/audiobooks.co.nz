//
//  ABAudiobookService.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 10/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper

// Get authentification
struct GetOrdersURLRequestProvider: URLRequestConvertible {
    let userID: Int
    init(userID: Int) {
        self.userID = userID
    }
    
    func asURLRequest() throws -> URLRequest {
        // https://audiobooksnz.co.nz/wc-api/v2/customers/22/orders?consumer_key=ck_67b008e3c474e39b8b72bcddda65af750e315de3&consumer_secret=cs_ab2c235ea41cc3a205a722a72658fe585b64e30f
        let orderString = String("\(Constants.Network.kBaseURL)/customers/\(userID)/orders?consumer_key=\(Constants.Network.kConsumerApiKey)&consumer_secret=\(Constants.Network.kConsumerApiSecret)")
        print("GetOrdersURLRequestProvider \(orderString)")
        return URLRequest(url: URL(string: orderString!)!)
    }
}

struct GetProductsFromOrderURLRequestProvider: URLRequestConvertible {
    let orderId: Int
    init(orderId: Int) {
        self.orderId = orderId
    }
    
    func asURLRequest() throws -> URLRequest {
        //https://audiobooksnz.co.nz/wc-api/v2/orders/12053?consumer_key=ck_67b008e3c474e39b8b72bcddda65af750e315de3&consumer_secret=cs_ab2c235ea41cc3a205a722a72658fe585b64e30f
        let productFromOrderString = String("\(Constants.Network.kBaseURL)/orders/\(orderId)?consumer_key=\(Constants.Network.kConsumerApiKey)&consumer_secret=\(Constants.Network.kConsumerApiSecret)")
        print("GetProductsFromOrderURLRequestProvider \(productFromOrderString)")
        return URLRequest(url: URL(string: productFromOrderString!)!)
    }
}

struct GetProductDetailURLRequestProvider: URLRequestConvertible {
    let productId: Int
    init(productId: Int) {
        self.productId = productId
    }
    
    func asURLRequest() throws -> URLRequest {
        //https://audiobooksnz.co.nz/wc-api/v2/products/11591?consumer_key=ck_67b008e3c474e39b8b72bcddda65af750e315de3&consumer_secret=cs_ab2c235ea41cc3a205a722a72658fe585b64e30f
        let productDetailsString = String("\(Constants.Network.kBaseURL)/products/\(productId)?consumer_key=\(Constants.Network.kConsumerApiKey)&consumer_secret=\(Constants.Network.kConsumerApiSecret)")
        print("GetProductDetailURLRequestProvider \(productDetailsString)")
        return URLRequest(url: URL(string: productDetailsString!)!)
    }
}

class ABAudiobookService : NSObject {
    override init() {
    }
    
    // We dont really need a mappable response for Orders as our main concern is to retrieve produts downloadurl and store it
    func getOrders(userID: Int, completion: @escaping (_ result: JSON) -> Void)-> () {
        let urlRequestProvider = GetOrdersURLRequestProvider(userID:userID)
        Alamofire.request(urlRequestProvider).responseJSON { response in
            if (response.result.isSuccess) {
                completion(JSON(response.result.value!)["orders"])
            }
            else {
                completion(JSON.null)
            }
        }
    }
    
    func getProducts(orderId: Int, completion: @escaping (_ result: JSON) -> Void)-> () {
        let urlRequestProvider = GetProductsFromOrderURLRequestProvider(orderId: orderId)
        
        Alamofire.request(urlRequestProvider).responseJSON { response in
            if (response.result.isSuccess) {
                completion(JSON(response.result.value!)["order"]["line_items"])
            }
            else {
                completion(JSON.null)
            }
        }
    }
    
    func getAudioBookDetails(productId: Int, completion: @escaping (_ result:AudioBook) -> Void)-> () {
        let urlRequestProvider = GetProductDetailURLRequestProvider(productId: productId)
        
        Alamofire.request(urlRequestProvider).responseObject { (response: DataResponse<AudioBook>) in
            let audioBook = response.result.value
            completion(audioBook!)
        }
    }
    
    func isContainingZip(type: String, audiobook: AudioBook) -> Bool {
        // Check the download
        for download in audiobook.downloads
        {
            let url = NSURL(string: download.file)
            print("Path for the file \(url) has path extension \(url?.pathExtension)")
        }
        return false
    }
    
    func availableFormat(audiobook: AudioBook) -> Array<String>{
        // Check the download
        var formats:Array<String> = []
        for download in audiobook.downloads
        {
            let url = NSURL(string: download.file)
            let pathExtension = url?.pathExtension
            if (formats.contains(pathExtension!) == false)
            {
                formats.append(pathExtension!)
            }
        }
        return formats
    }
    
    func metaTypeForFormats(formats: Array<String>) -> String {
        var formatsAvailable = formats
        if formatsAvailable.contains("zip") {
            let indexZipFormat = formatsAvailable.index(of: "zip")
            formatsAvailable.remove(at: indexZipFormat!)
        }
        
        if formatsAvailable.count > 1 {
            return ""
        }
        
        if formatsAvailable.count == 1
        {
            return formatsAvailable.first!
        }
        
        return ""
    }
    
    func getAllOrders(userID: Int, completion: @escaping (_ result: [AudioBook]) -> Void)-> () {
        // Store all the product available
        var products = [AudioBook]()
        
        let ordersFoundGroup = DispatchGroup()
        
        self.getOrders(userID: userID) { (ordersJson) in
            for order in ordersJson.array! {
                let orderIdFound = order["id"].intValue
                if orderIdFound > 0 {
                    ordersFoundGroup.enter()
                    print("----- \(orderIdFound) -----")
                    self.getProducts(orderId: orderIdFound, completion: { (productsJson) in
                        let productFoundGroup = DispatchGroup()
                        
                        // We need to filter only MP3 product
//                        let productsJsonMp3OrM4b = productsJson.array!.filter {$0["meta"][0]["value"] == "MP3" || $0["meta"][0]["value"] == "M4B"}
                        let productsJsonMp3OrM4b = productsJson.array!
                        for product in productsJsonMp3OrM4b {
                            let productId = product["product_id"].intValue
                            let productName = product["name"].string
                            let productMetaType = product["meta"][0]["value"].stringValue
                            print("productId found \(productName) : \(productId) \(productMetaType)")
                            productFoundGroup.enter()
                            self.getAudioBookDetails(productId: productId, completion: { (audioBooks) in
                                
                                let formats = self.availableFormat(audiobook: audioBooks)
                                print("available format \(formats) for \(audioBooks.title)")
                                let metaType = self.metaTypeForFormats(formats: formats)
                                audioBooks.type = metaType
                                products.append(audioBooks)
                                productFoundGroup.leave()
                            })
                        }
                        
                        productFoundGroup.notify(queue: .main) {
                            ordersFoundGroup.leave()
                        }
                    })
                }
            }
            
            ordersFoundGroup.notify(queue: .main) {
                completion(products)
            }
        }
    }
}
