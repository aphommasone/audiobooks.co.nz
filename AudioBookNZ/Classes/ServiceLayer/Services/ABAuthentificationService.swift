//
//  ABAuthentificationService.swift
//  
//
//  Created by Arnaud Phommasone on 10/05/17.
//
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireObjectMapper

// Get authentification
struct GetAuthentificationURLRequestProvider: URLRequestConvertible {
    let username: String
    let password: String
    init(username: String, password: String) {
        self.username = username
        self.password = password
    }
    
    func asURLRequest() throws -> URLRequest {
        
        let authentificationString = String("\(Constants.Network.kCustomEndPointBaseURL)/authenticate?username=\(username)&password=\(password)" )
        let authentificationEscapedString = authentificationString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        return URLRequest(url: URL(string: authentificationEscapedString!)!)
    }
}


class ABAuthentificationService : NSObject {
    override init() {
        
    }
    
    func getAuthentificationResult(username: String, password: String, completion: @escaping (_ result: User?) -> Void)-> () {
        let urlRequestProvider = GetAuthentificationURLRequestProvider(username: username, password: password)
        
        Alamofire.request(urlRequestProvider).responseObject { (response: DataResponse<User>) in
            let responseUser = response.result.value
            completion(responseUser)
        }
    }
}
