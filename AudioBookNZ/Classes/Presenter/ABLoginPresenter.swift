//
//  OrderListPresenter.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 16/05/17.
//  Copyright 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift

protocol ABAuthentificationView: NSObjectProtocol {
    func credentialsDidSucceeded(user: User);
    func credentialsDidFail();
}


class AuthentificationPresenter {
    private let authentificationService:ABAuthentificationService
    weak private var loginView : ABLoginViewController?
    
    init(authentificationService: ABAuthentificationService) {
        self.authentificationService = authentificationService
    }
    
    func attachView(view:ABLoginViewController) {
        loginView = view
    }
    
    func detachView() {
        loginView = nil
    }
    
    func getDemoCredentials() ->  (String, String) {
        let username = Constants.Demo.kDemoCredentialsUsername
        let password = Constants.Demo.kDemoCredentialsPassword
        
        // See if the value exist
        return (username, password)
    }
    
    func getStoredCredentials() -> (String, String)? {
        if (self.firstTimeAppLaunched())
        {
            UserDefaults.standard.set(false, forKey: Constants.PreferencesKey.kPreferencesOpenAppFirstTime)
            UserDefaults.standard.synchronize()
            
            return getDemoCredentials()
        }
        
        let username = UserDefaults.standard.string(forKey: Constants.PreferencesKey.kPreferencesCredentialsUsername)
        let password = UserDefaults.standard.string(forKey: Constants.PreferencesKey.kPreferencesCredentialsPassword)
   
        // See if the value exist
        if (username == nil || password == nil) {
            return ("", "")
        }
        else {
            return (username!, password!)
        }
    }
    
    func emptyStoredCredentials() {
        UserDefaults.standard.removeObject(forKey: Constants.PreferencesKey.kPreferencesCredentialsUsername)
        UserDefaults.standard.removeObject(forKey: Constants.PreferencesKey.kPreferencesCredentialsPassword)
        UserDefaults.standard.synchronize()
    }
    
    func firstTimeAppLaunched() -> Bool {
        if ( UserDefaults.standard.object(forKey: Constants.PreferencesKey.kPreferencesOpenAppFirstTime) == nil) {
            UserDefaults.standard.set(true, forKey: Constants.PreferencesKey.kPreferencesOpenAppFirstTime)
            UserDefaults.standard.synchronize()
        }
        return UserDefaults.standard.bool(forKey: Constants.PreferencesKey.kPreferencesOpenAppFirstTime)
    }
    
    func getUserCredentials(username: String, password: String){
        
        if (Connectivity.isConnectedToInternet()) {
            LoadingOverlay.shared.showOverlay()
            
            // We will check if it's the first time user launched the app
            if (self.firstTimeAppLaunched() )
            {
                
            }
            authentificationService.getAuthentificationResult(username: username, password: password, completion:  { (userResponse) in
                // if the User retrieved has no ID, it means the username / password doesnt match or not exist on the website
                if userResponse == nil {
                    self.loginView?.credentialsDidFail()
                }
                else {
                    if (userResponse!.userId > 0) {
                        self.loginView?.credentialsDidSucceeded(user: userResponse!)
                        
                        // Store the credentials
                        UserDefaults.standard.set(username, forKey: Constants.PreferencesKey.kPreferencesCredentialsUsername)
                        UserDefaults.standard.set(password, forKey: Constants.PreferencesKey.kPreferencesCredentialsPassword)
                        UserDefaults.standard.synchronize()
                    }
                    else {
                        self.loginView?.credentialsDidFail()
                    }
                }
                
                LoadingOverlay.shared.hideOverlayView()
            })
        } else {
            Connectivity.handleNoInternetConnection() //Display error message
        }
    }
}
