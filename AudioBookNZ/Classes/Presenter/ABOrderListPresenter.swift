//
//  OrderListPresenter.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 16/05/17.
//  Copyright 2017 Arnaud. All rights reserved.
//

import Foundation
import RealmSwift
import MessageUI

protocol ABOrderListView: NSObjectProtocol {
    func loadTableView(audiobook: [AudioBook]) //Load data into TableView
    func goBack()
    func createdMailComposer(mail: MFMailComposeViewController)
}

class OrderListMailComposerPresenter: NSObject, MFMailComposeViewControllerDelegate
{
    // Extension
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true) {
            controller.dismiss(animated: false, completion: nil)
        }
    }
}


class OrderListPresenter {
    private let audioBookService:ABAudiobookService
    weak private var orderListView : ABOrderListView?
    
    private var mailComposerDelegater:OrderListMailComposerPresenter = OrderListMailComposerPresenter()
    
    // URL of social apps
    let appTwitterURL = URL(string: "twitter://user?screen_name=\(Constants.Social.kTwitterUsername)")!
    let webTwitterURL = URL(string: "https://twitter.com/\(Constants.Social.kTwitterUsername)")!
    
    let appFacebookURL = URL(string: "fb://profile/\(Constants.Social.kFacebookID)")!
    let webFacebookURL = URL(string: "https://facebook.com/\(Constants.Social.kFacebookPageName)")!
    
    // Email enquiries
    let contactSupportMail = "support@audiobooksnz.co.nz"
    let subjectMail = "App Question"
    
    init(audioBookService: ABAudiobookService) {
        self.audioBookService = audioBookService
    }
    
    func attachView(view:ABOrderListView) {
        orderListView = view
    }
    
    func detachView() {
        orderListView = nil
    }
    
    func getAudioBooks(userId: Int){
        if (Connectivity.isConnectedToInternet()) {
            LoadingOverlay.shared.showOverlay()
            // Test users 2 ID user logged in : User(userId: "27", name: "testuser2@audiobooksnz.co.nz", email: "testuser2@audiobooksnz.co.nz")
            audioBookService.getAllOrders(userID: userId) { audioBooks in
                LoadingOverlay.shared.hideOverlayView()
                self.saveAudioBooks(audiobooks: audioBooks, forUser: userId)
                self.orderListView?.loadTableView(audiobook: audioBooks)
            }
        } else {
            Connectivity.handleNoInternetConnection() //Display error message
            // try to get the local one
            LoadingOverlay.shared.showOverlay()
            let audiobooks: [AudioBook] = self.getOfflineAudioBooks(userId: userId).toArray() as! [AudioBook]
            self.orderListView?.loadTableView(audiobook: audiobooks)
            LoadingOverlay.shared.hideOverlayView()
        }
    }
    
    func saveAudioBooks(audiobooks: [AudioBook], forUser: Int) {
        do {
            let realm = try Realm()
            try realm.write {
                for audiobook in audiobooks {
                    audiobook.userId = forUser
                    realm.add(audiobook, update: true)
                }
            }
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
    
    func getOfflineAudioBooks(userId: Int)-> Results<AudioBook> {
        var audiobooks: Results<AudioBook>
        do {
            let realm = try Realm()
            let predicate = "userId == \(userId)"
            audiobooks = realm.objects(AudioBook.self).filter(predicate)
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
        return audiobooks
    }
    
    func logout() {
        LoadingOverlay.shared.showOverlay()
        // We fake a disconnection but in reality we just pop back the navigation controller
        let dispatchTime = DispatchTime.now() + .seconds(2)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            self.orderListView?.goBack()
            LoadingOverlay.shared.hideOverlayView()
        }
    }
    
    func refresh() {
        
    }
    
    // Facebok redirect
    func goFacebook() {
        let application = UIApplication.shared
        
        if application.canOpenURL(appFacebookURL) {
            application.openURL(appFacebookURL)
        } else {
            application.openURL(webFacebookURL)
        }
    }
    
    func isFacebookAppAvailable() -> Bool {
        let application = UIApplication.shared
        return application.canOpenURL(appFacebookURL)
    }
    
    // Twitter redirect
    func goTwitter() {
        let application = UIApplication.shared
        if application.canOpenURL(appTwitterURL) {
            application.openURL(appTwitterURL)
        } else {
            application.openURL(webTwitterURL)
        }
    }
    
    // Show Email
    func goEmail() {
        let mc = MFMailComposeViewController()
        mc.modalPresentationStyle = .overCurrentContext
        mc.mailComposeDelegate = self.mailComposerDelegater
        
        // Get email, subject
        let email = Constants.Mail.kEmail
        let subject = Constants.Mail.kSubject
        mc.setSubject(subject)
        mc.setMessageBody("", isHTML: false)
        mc.setToRecipients([email])
        mc.modalTransitionStyle = .coverVertical
        
        self.orderListView?.createdMailComposer(mail: mc)
    }
    
    func isTwitterAppAvailable() -> Bool {
        let application = UIApplication.shared
        return application.canOpenURL(appTwitterURL)
    }
    
    func notifViewWillDisappear(_from viewController: UIViewController) {
        // Grab the destination
        let destination = viewController.parent?.childViewControllers.last
        if destination?.isKind(of: ABLoginViewController) == true {
            let loginDestination = destination as! ABLoginViewController
            loginDestination.preventAutoLoggingIn = true
        }
    }
}
