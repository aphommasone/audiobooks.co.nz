//
//  ABDownloadListPresenter.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 14/06/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation
import Alamofire
import Realm
import RealmSwift
import AVFoundation
import MediaPlayer

protocol ABDownloadListView: NSObjectProtocol {
    func prepareAudiobookPlayer()
    func enableFormatSelection(enabled: Bool)
}

class DownloadListPresenter {
    private let downloadService:ABDownloadService
    weak private var downloadListView : ABDownloadListView?
    
    private var lockedDownloadedTableViewCell : Array<ABDownloadTableViewCell> = []
    
    init(downloadService: ABDownloadService) {
        self.downloadService = downloadService
    }
    
    func attachView(view:ABDownloadListView) {
        downloadListView = view
    }
    
    func detachView() {
        downloadListView = nil
    }
    // If download available
    func getDownloadSize(download: AudioBookDownloads) -> String {
        let documentsURL = download.pathInDocuments()
        do {
            try documentsURL.checkResourceIsReachable()
            let attr:NSDictionary? = try FileManager.default.attributesOfItem(atPath: documentsURL.path) as NSDictionary
            if attr != nil {
                let fileSize = ByteCountFormatter.string(fromByteCount: Int64((attr?.fileSize())!), countStyle: .file)
                return fileSize
            }
            return "0 MB"
        } catch let error as NSError {
            print(error)
            return "0 MB"
        }
    }
    
    func downloadAvailable(download: AudioBookDownloads) -> Bool {
        let documentsURL = download.pathInDocuments()
        do {
            try documentsURL.checkResourceIsReachable()
            // handle the boolean result
            return true
        } catch let error as NSError {
            print(error)
            return false
        }
        return false
    }
    
    func removeAllDownloads(audiobook: AudioBook) {
        for downloads in audiobook.downloads {
            do {
                let documentsURL = downloads.pathInDocuments()
                try documentsURL.checkResourceIsReachable()
                
                // Remove the downloads
                try FileManager.default.removeItem(at: documentsURL)
            } catch let error as NSError {
                print(error)
            }
            
        }
    }
    
    func getChaptersFromDownloads(downloads: [AudioBookDownloads]) -> [[Chapter]]
    {
        var allChapters: [[Chapter]] = []
        for download in downloads {
            // Check if download is available first
            if (self.downloadAvailable(download: download) ) {
                let audioboksUrl = Helper.shared.getAudioPathURL(download: download)
                let chapters: [Chapter] =  Helper.shared.retrieveChaptersM4B(asset: AVAsset(url: audioboksUrl!))
                allChapters.append(chapters)
            }
            else {
                // just return empty for everyone
                return []
            }
        }
        
        return allChapters
    }
    
    func download(audiobook: AudioBookDownloads, cell: ABDownloadTableViewCell) {
        // Check if download available 
        if (downloadAvailable(download: audiobook)) {
            // Code for calling the player
            self.downloadListView?.prepareAudiobookPlayer()
        }
        else if (self.lockedDownloadedTableViewCell.contains(cell)) {
            // Do nothing
        }
        // If not download it
        else {
            
            // Enable or not the segmented control for the different audio format
            self.downloadListView?.enableFormatSelection(enabled: false)
            
            let downloadName = audiobook.name
            //audioUrl should be of type URL
            
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                documentsURL.appendPathComponent(downloadName)
                return (documentsURL, [.removePreviousFile])
            }
            
            self.lockedDownloadedTableViewCell.append(cell)
            
            Alamofire.download(audiobook.file, to: destination).downloadProgress(closure: { (progress) in
                //progress closure
                if (progress.isIndeterminate)
                {
                    cell.updateUI(forState: .DownloadUndetermined)
                    let fileSize = ByteCountFormatter.string(fromByteCount: progress.completedUnitCount, countStyle: .file)
                   cell.updateDownloadInfoLabel(size: fileSize)
                }
                else
                {
                    cell.updateUI(forState: .Downloading)
                    cell.progressView.progress = Float(progress.fractionCompleted)
                }
            }).response { response in
                print("download finished :\(audiobook.file) : \(response.destinationURL)")
                
                let filemanager = FileManager.default
                
                do
                {
                    let responseURL = response.destinationURL
                    
                    let newPath = audiobook.pathInDocuments()
                    
                    try filemanager.moveItem(atPath: (response.destinationURL?.path)!, toPath: newPath.path)
                    print("newPath : \(newPath)")
                }
                catch let error as NSError
                {
                    print("error : \(error)")
                }
                
                let indexToRemove = self.lockedDownloadedTableViewCell.index(of: cell)
                self.lockedDownloadedTableViewCell.remove(at: indexToRemove!)
                
                // Append the size of the downloads
                let size = self.getDownloadSize(download: audiobook)
                cell.nameLabel.text = cell.nameLabel.text! + "(\(size))"
                cell.updateUI(forState: .Downloaded)
                
                // Enable or not the segmented control for the different audio format
                self.downloadListView?.enableFormatSelection(enabled: self.lockedDownloadedTableViewCell.count == 0)
            }
        }
    }
}
