//
//  Prompt.swift
//  AudioBookNZ
//
//  Created by aphommasone on 27/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import Foundation


// Constant : this can stay here
struct Constants{
    struct Animation {
        static let kBottomButtonTimespeed = 0.3
        static let kBottomClosedConstraintValue = -86
        static let kBottomOpenedConstraintValue = 0
    }
    
    struct Social {
        static let kTwitterUsername = "audiobooksnz"
        static let kFacebookID = "PUT_HERE"
        static let kFacebookPageName = "audiobooksnz"
    }
    
    struct Mail {
        static let kEmail = "support@audiobooksnz.co.nz"
        static let kSubject = "App Question"
    }
    
    struct PreferencesKey {
        static let kPreferencesCredentialsUsername = "ABUserCredentialsName"
        static let kPreferencesCredentialsPassword = "ABUserCredentialsPassword"
        
        static let kPreferencesOpenAppFirstTime = "ABOpenAppFirstTime"
    }
    
    struct Demo {
        static let kDemoCredentialsUsername = "Demo"
        static let kDemoCredentialsPassword = "Demouser2017"
    }
    
    struct Network {
        static let kTwitterURL = "https://twitter.com/facebook"
        
        static let kBaseURL = "https://audiobooksnz.co.nz/wc-api/v2"
        static let kCustomEndPointBaseURL = "https://audiobooksnz.co.nz/wp-json/audiobooknz-api/v1"
        static let kConsumerApiKey = "ck_67b008e3c474e39b8b72bcddda65af750e315de3"
        static let kConsumerApiSecret = "cs_ab2c235ea41cc3a205a722a72658fe585b64e30f"
        
        static let kWebsiteURL = "https://audiobooksnz.co.nz"
        static let kWebsiteForgotPasswordURL = "https://audiobooksnz.co.nz/my-account/lost-password/?v=8e3eb2c69a18"
        static let kWebsiteRegisterURL = "https://audiobooksnz.co.nz/registration-page/"
    }
    
    struct Text {
        static let kTextNoProductsAvailable = "We were not able to retrieve any products related to that account. You can have our products by visiting our website"
        static let kTextDownloadedLabel = "Downloaded"
        static let kTextNotDownloadedLabel = "Not downloaded"
    }
    
    struct AudioPlayer {
        static let kPlayerThrowbackSeconds: Float = 5
        static let kPlayerJumpNextSeconds: Float = 60
        static let kPlayerJumpPrevSeconds: Float = -60
    }
    
    struct AlertPrompt {
        static let kInvalidCredentialsMessage = "The username or password you entered is incorrect"
        static let kInvalidCredentialsTitle = "Credentials error"
        static let kLogoutWarningMessage = "Are you sure you want to logout of the application ?"
        static let kLogoutWarningTitle = "Warning"
        static let kNoNetworkConnectivityMessage = "You do not have network connectivity. Please check your internet connection and try again."
        static let kNoNetworkConnectivityTitle = "Internet error"
        static let kRemoveDownloadsMessage = "Are you sure you want to remove all saved downloads ?"
        static let kRemoveDownloadsWarningTitle = "Warning"
    }
}
