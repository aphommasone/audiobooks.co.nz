//
//  ABLoadingOverlay.swift
//  AudioBookNZ
//
//  Created by Arnaud Phommasone on 31/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import KDLoadingView
import UIKit

public class LoadingOverlay{
    
    var overlayView = UIView()
    var loadingView = KDLoadingView()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay() {
        
        if  let appDelegate = UIApplication.shared.delegate as? AppDelegate,
            let window = appDelegate.window {
            
            overlayView.frame = CGRect(x: 0, y: 0, width: window.frame.size.width, height: window.frame.size.height)
            overlayView.center = CGPoint(x: window.frame.width/2, y: window.frame.height/2)
            overlayView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            overlayView.backgroundColor = UIColor.init(colorLiteralRed: 104/255, green: 104/255, blue: 104/255, alpha: 0.4)
            
            
            let loadingViewSize = 60 as CGFloat
            let originx = window.frame.size.width/2 - loadingViewSize/2
            let originy = window.frame.size.height/2 - loadingViewSize/2
            
            loadingView.frame = CGRect(x: originx, y: originy, width: loadingViewSize, height: loadingViewSize)
            
            loadingView.autoresizingMask = [.flexibleLeftMargin, .flexibleTopMargin, .flexibleRightMargin, .flexibleBottomMargin]
            loadingView.firstColor = UIColor.init(colorLiteralRed: 176/255, green: 48/255, blue: 71/255, alpha: 1)
            loadingView.lineWidth = 3
            
            overlayView.addSubview(loadingView)
            window.addSubview(overlayView)
            
            loadingView.startAnimating()
        }
    }
    
    public func hideOverlayView() {
        loadingView.stopAnimating()
        overlayView.removeFromSuperview()
    }
}
