//
//  ABDownloadTableViewCell.swift
//  AudioBookNZ
//
//  Created by aphommasone on 27/05/17.
//  Copyright © 2017 Arnaud. All rights reserved.
//

import UIKit

enum ABDownloadTableViewCellState {
    case NotDownloaded
    case Downloading
    case DownloadUndetermined
    case Downloaded
}

class ABDownloadTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    // THis is used only when the total size cant be retrieved from the header server info
    @IBOutlet weak var downloadedSizeInfoLabel: UILabel!
    
    @IBOutlet weak var statusReadingChapterLabel: UILabel!
    var currentState: ABDownloadTableViewCellState = .NotDownloaded
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.progressView.progress = 0
        self.progressView.progressTintColor = UIColor(red: (209/255.0), green: (48/255.0), blue: (71/255.0), alpha: 1.0)
        
        self.statusReadingChapterLabel.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: UI functions
    func updateUI(forState: ABDownloadTableViewCellState, isSizeDefined: Bool) {
        updateUI(forState: forState)
        if (!isSizeDefined) {
            self.progressView.isHidden = true
            
        }
    }
    
    func updateDownloadInfoLabel(size: String) {
        self.downloadedSizeInfoLabel.text = "Downloading \(size)"
    }
    
    func updateUI(forState: ABDownloadTableViewCellState) {
        self.downloadedSizeInfoLabel.isHidden = true
        switch forState {
        case .NotDownloaded:
            self.nameLabel.textColor = UIColor.lightGray
            self.progressView.isHidden = true
            
            break
        case .Downloading:
            self.progressView.isHidden = false
            break
        case .DownloadUndetermined:
            self.progressView.isHidden = true
            self.downloadedSizeInfoLabel.isHidden = false
            break
        case .Downloaded:
            self.nameLabel.textColor = UIColor.black
            self.progressView.isHidden = true
            break
        default:
            self.nameLabel.textColor = UIColor.black
            self.progressView.isHidden = false
        }
    }
}
